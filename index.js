//JavaScript Objects
    //similar to an array, it also contains multiple values.
    //unlike array that uses index, objects use properties.
    //with object, we can easily give lables to each value.

    /*
        Structure or Syntax

        let objectName = {
                key:value;,
                key: (function),
                key: object {
                    key:value;
                },
                key:[array]
        }
    */

let cellphone = {
    name:"Nokia 3210",
    ManufactureDate: 1999.  
};

console.log(cellphone)
console.log(typeof cellphone)

//creating objects using a construtor function
    /*
        Syntax;
        function objectName(keyA, keyB){
            this.keyA = keyA;
            this.keyB = keyB;
        }
    */

function Laptop(name,ManufactureDate) {
  this.name = name;
  this.releaseDate = ManufactureDate;  
};

let laptop = new Laptop("Lenovo","2008");
console.log(laptop);
//will create new object
//new operator create s an instance of an aobject

let laptop1 = Laptop("Asus","2010");
console.log(laptop1);// undefined

//Create empty objects
let computer = {};
let myComputer = new Object();

console.log(computer);
console.log(myComputer);

//Accessing Array Objects

let machines = [laptop,laptop1]

    //access the property of an object inside an array

    console.log(machines[0].name)//lenovo

    //another way of accessing properties in an array
    console.log(machines[0]["releaseDate"])//2008

//Initializing/ Adding / Deleting / Reassigning object properties

let car = {};

//initializing / adding object properties using dot notation

car.name = "Honda Civic";

console.log(car)

// adding object properties with square brackets
car["manufactureDate"] = 2019;
console.log(car)

//Deleting Object properties
delete car["name"];
console.log(car)

car.brand = "Totota";
car.model = "Wigo";
car.color = "Gray Metallic";
car.location = "Toyota Zamboanga City";
console.log(car);

//Reassigning object properties
car.manufactureDate = 1986;
console.log(car)


//Object Methods
    //a method is function which is a property of an object
    //they are also functions and one of the 

    //meethods are defined based on what an object is capable of doing and how it should work.

    let person = {
        name: "jin",
        talk: function(){
                console.log(`hello my name is ${this.name}`)
        }
    };
    
    person.talk();


    let friend = {
        firstName: "Bam",
        LastName: "Jeon",
        address: {
            city: "Caloocan",
            country: "Philippines"
        },
        emails: ["bam@gmail.com", "jeonbam@gmail.com"],
        introduce: function(){
            console.log (`Hello my name is ${this.firstName} ${this.LastName}, i live in ${this.address.city}, ${this.address.country}. My personal email is ${this.emails[0]}`)
        }
    };
    friend.introduce();

    //Real World Application
        //Scenario:
            //1. Create a game that would have several pokemons to interact with each other.
            //2. Every pokemon should have stats, properties, function

    let myPokemon = {
        name: "Bulbasaur",
        level: 3,
        health: 100,
        attack: 50,
        tackle: function (){
            console.log(`${this.name} tackled another Pokemon`)
            console.log(`targetPokemon's Pokemon Health is now reduced`)
        },
        faint: function (){
            console.log(`${this.name} fainted`);
        }
    
    };
    myPokemon.tackle();
    myPokemon.faint();


// CREATING OBJECT WITH AN OBJECT CONSTRUCTOR

function Pokemon (name, lvl, hp){
    //Properties
    this.name = name;
    this.lvl = lvl;
    this.health = hp * 2;
    this.attack = lvl;

    //methods
    this.tackle = function(target){
        console.log(`${this.name} tackled ${target.name}`);
       
        target.health = target.health - this.attack;
        
        
        console.log(`${target.name} Pokemon's health is now reduced to ${target.health}`);

        if(target.health <= 10){
            target.faint(target);
        }
    }

    this.faint = function(target){
        console.log(`${this.name} fainted`)
    }
};
//create new instances of the Pokemon object each with their unique properties

let pikachu = new Pokemon ("Pikachu", 3, 50);
let ratata = new Pokemon ("Ratata", 5, 25);
let charmander = new Pokemon ("Charmander", 13, 75)

//providing the "ratata" object as an argument to "pikachu";

// pikachu.tackle(ratata)
// pikachu.tackle(ratata)
// pikachu.tackle(ratata)
// pikachu.tackle(ratata)

charmander.tackle(pikachu);
pikachu.tackle(charmander);
charmander.tackle(pikachu);
pikachu.tackle(charmander);
charmander.tackle(pikachu);
pikachu.tackle(charmander);
charmander.tackle(pikachu);
pikachu.tackle(charmander);
charmander.tackle(pikachu);
pikachu.tackle(charmander);
charmander.tackle(pikachu);
charmander.tackle(pikachu);



