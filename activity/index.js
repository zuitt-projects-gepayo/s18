function Pokemon (name, lvl, hp){
    //Properties
    this.name = name;
    this.lvl = lvl;
    this.health = hp * 2;
    this.attack = lvl;

    //methods
    this.tackle = function(target){
        console.log(`${this.name} tackled ${target.name}`);
       
        target.health = target.health - this.attack;
        
        
        console.log(`${target.name} Pokemon's health is now reduced to ${target.health}`);

        if(target.health < 10){
            target.faint(target);
        }
    }

    this.faint = function(target){
        console.log(`${target.name} fainted`)
    }
};
//create new instances of the Pokemon object each with their unique properties

let pikachu = new Pokemon ("Pikachu", 3, 50);
let ratata = new Pokemon ("Ratata", 5, 25);
let charmander = new Pokemon ("Charmander", 10, 75)


//providing the "ratata" object as an argument to "pikachu";

// pikachu.tackle(ratata);
// ratata.tackle(pikachu);
// pikachu.tackle(ratata);
// ratata.tackle(pikachu);
// pikachu.tackle(ratata);
// ratata.tackle(pikachu);
// pikachu.tackle(ratata);
// pikachu.tackle(ratata);
// ratata.tackle(pikachu);
// pikachu.tackle(ratata);
// ratata.tackle(pikachu);
// pikachu.tackle(ratata);
// ratata.tackle(pikachu);
// pikachu.tackle(ratata);
// pikachu.tackle(ratata);
// ratata.tackle(pikachu);
// pikachu.tackle(ratata);
// ratata.tackle(pikachu);
// pikachu.tackle(ratata);
// ratata.tackle(pikachu);
// pikachu.tackle(ratata);
// ratata.tackle(pikachu);
// pikachu.tackle(ratata);
// ratata.tackle(pikachu);
// pikachu.tackle(ratata);

// charmander.tackle(pikachu)
// pikachu.tackle(charmander)
// charmander.tackle(pikachu)
// pikachu.tackle(charmander)
// charmander.tackle(pikachu)
// pikachu.tackle(charmander)
// charmander.tackle(pikachu)
// pikachu.tackle(charmander)
// charmander.tackle(pikachu)
// pikachu.tackle(charmander)
// charmander.tackle(pikachu)
// pikachu.tackle(charmander)
// charmander.tackle(pikachu)
// pikachu.tackle(charmander)
// charmander.tackle(pikachu)
// pikachu.tackle(charmander)
// charmander.tackle(pikachu)
// pikachu.tackle(charmander)
// charmander.tackle(pikachu)

charmander.tackle(ratata);
ratata.tackle(charmander);
charmander.tackle(ratata);
ratata.tackle(charmander);
charmander.tackle(ratata);
ratata.tackle(charmander);
charmander.tackle(ratata);
ratata.tackle(charmander);
charmander.tackle(ratata);